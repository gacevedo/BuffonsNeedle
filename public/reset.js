function setup() {
  const one_pin = select("#big_button");
  //socket = io.connect("http://localhost:3000"); //socket server local.
  socket = io.connect("http://34.205.65.120:3000"); //socket serve

  one_pin.mousePressed(() => {
    //do something, send the code
    sendData();
  });
}
function sendData() {
  const dataToSend = {
    x1: -1,
    y1: -1,
    angle: 1,
    intersect: -1,
    first: -1,
  };

  socket.emit("pinEvent", dataToSend);
}
