let cv; //canvas
const CSD = 1.5; //canvasSizeDivider
var multiplePins = 0; //how many pins to drop
var rememberPin = [0, 0, 0, 0]; //last pin that was successful
var eraseFlag = false; // Do we need to erase  highlighting
let socket;
var dataSend = [0, 0, 0, 0, 0, 0, 0, 0]; //x,y,x,y,intersect,width,height,firstTime
var dataRecieve = [0, 0, 0, 0, 0, 0, 0]; // x,y,x,y,intersect,width,height
var firstTime = 0;
var totalPinsDropped = 0;
var totalIntersects = 0;
var currentPi = 0.0001;
var pinArray = [];
var indivPins = [0, 0, 0, 0, 0]; // x1,y1,x2,y2,local or Global?
var hit = 0;
pinArray.push(indivPins);

function setup() {
  firstTime = 1;
  cv = createCanvas(windowWidth / CSD, windowHeight / CSD);
  centerCanvas();
  const one_pin = select("#one_pin");
  const twenty_pins = select("#twenty_pins");
  cv.background("#1a1a1d");
  //choose one of the following options:
  //socket = io.connect("http://localhost:3000"); //socket server local.
  socket = io.connect("http://34.205.65.120:3000"); //socket server
  socket.on("pinEmit", (serverData) => {
    //recieved emit data from server
    // console.log("serverData ", serverData);
    if (serverData.x1 == -1) {
      pinArray.length = 0;
    }
    totalPinsDropped = serverData.ttlPins;
    totalIntersects = serverData.ttlIntersects;
    drawBackgroundPin(serverData);
  });
  one_pin.mousePressed(() => {
    // this tells the draw to drop a pin
    multiplePins = 1; // only on pin please
  });
  twenty_pins.mousePressed(() => {
    // this tells the draw to drop a pin
    multiplePins = 20;
  });
}

function drawBackgroundPin(serverData) {
  x1 = (serverData.x1 / serverData.wdth) * width;
  y1 = (serverData.y1 / serverData.hight) * height;
  x2 = (serverData.x2 / serverData.wdth) * width;
  y2 = (serverData.y2 / serverData.hight) * height;

  pinArray.unshift(x1, y1, x2, y2, 0);

  stroke("Gray");
  line(x1, y1, x2, y2);
}

function draw() {
  //main loop
  cv.background("#1a1a1d");
  // draw the two lines
  for (i = 0; i < 300; i += 5) {
    //draw 300 background lines
    stroke("gray");
    line(pinArray[i], pinArray[i + 1], pinArray[i + 2], pinArray[i + 3]);
    if (i + 4 > 300) {
      break;
    }
  }
  stroke("#950740");
  strokeWeight(1.5);
  line(0, height / 3, width, height / 3);
  line(0, height / 1.5, width, height / 1.5);
  strokeWeight(1);
  // end two lines
  if (eraseFlag) {
    unhighlightPin(
      rememberPin[0],
      rememberPin[1],
      rememberPin[2],
      rememberPin[3]
    );
  }
  if (multiplePins > 0) {
    // drop one pin per draw loop
    DropPin();
    multiplePins--;
  }
  //add numbers to page here
  document.getElementById("ttlPins").innerHTML = totalPinsDropped;
  document.getElementById("Intersects").innerHTML = totalIntersects;
  currentPi = 2 * (totalPinsDropped / totalIntersects);
  document.getElementById("piCalc").innerHTML = currentPi;
  sleep(300);
}
//! helper functions
function DropPin() {
  var pinLocation = RandomPin();
  stroke("yellow");
  DrawPin(pinLocation[0], pinLocation[1], pinLocation[2], pinLocation[3]);
}
function Cointoss() {
  x = Math.random();
  result = -1;
  if (x < 0.5) {
    result = 1;
  }

  return result;
}
function Intersection() {
  inter = height / 3;
  if (Cointoss() == -1) {
    inter = height / 1.5;
  }
  return inter;
}
function RandomPin() {
  //
  hit = 0;
  theta = Math.random() * Math.PI;
  y1 = 0.5 * Math.sin(theta);

  y2 = y1 * -1;
  distance = Math.random() * 0.5; // distance from the intersection
  if (y1 > distance) {
    hit = 1;
  }
  realy1 = y1 + distance; //this translates the y axis to the correct place relative to the 'distance'
  realy2 = y2 + distance;
  scaledy1 = (realy1 * height) / 3; // height/3 is the length of the pin
  scaledy2 = (realy2 * height) / 3;
  ct = Cointoss();
  hitline = Intersection();
  // console.log("realy1, scaledy1, hitline ", realy1, scaledy1, hitline);
  placedy1 = ct * scaledy1 + hitline;
  placedy2 = ct * scaledy2 + hitline;
  // just to not have to refactor the whole thing
  y1 = placedy1;
  y2 = placedy2;
  angle = theta;
  randomOrigin = Math.random() * width;
  x1 = (height / 6) * Math.cos(theta) + randomOrigin;
  x2 = randomOrigin - (height / 6) * cos(theta);
  if (x2 < 0 || x2 > width || x1 < 0 || x1 > width) {
    do {
      plus = Math.random() * (width / 3) * Cointoss();
      x1 = x1 + plus;
      x2 = x2 + plus;
    } while (x2 < 0 || x2 > width || x1 < 0 || x1 > width);
  }
  if (y2 < 0 || y1 < 0) {
    y2 = y2 + height / 3;
    y1 = y1 + height / 3;
  }
  if (y1 > height || y2 > height) {
    y1 = y1 - height / 3;
    y2 = y2 - height / 3;

    // redo if out of bounds
  }
  dataSend[0] = x1;
  dataSend[1] = y1;
  dataSend[2] = x2;
  dataSend[3] = y2;
  dataSend[4] = hit;
  dataSend[5] = width;
  dataSend[6] = height;
  return [x1, y1, x2, y2, hit];
}

function DrawPin(x1, y1, x2, y2) {
  line(x1, y1, x2, y2);

  if (dataSend[4] == 1) {
    // this is the flag that signifies this was a hit, an intersection
    highlightPin(x1, y1, x2, y2);
    rememberPin = [x1, y1, x2, y2];
    eraseFlag = true;
  }
  dataSend[7] = firstTime;
  sendData(
    dataSend[0],
    dataSend[1],
    dataSend[2],
    dataSend[3],
    dataSend[4],
    dataSend[5],
    dataSend[6],
    dataSend[7]
  ); //
}
function sendData(x, y, x2, y2, intersect, wdth, hight, first) {
  const dataToSend = {
    x1: x,
    y1: y,
    x2: x2,
    y2: y2,
    intersect: intersect,
    wdth: wdth,
    hight: hight,
    first: first,
  };
  socket.emit("pinEvent", dataToSend);
  //console.log("just sent ", dataToSend);

  firstTime = 0;
}
function highlightPin(x1, y1, x2, y2) {
  stroke("Aqua");
  strokeWeight(5);
  line(x1, y1, x2, y2);
  rememberPin = [x1, y1, x2, y2];

  strokeWeight(1);
}
function unhighlightPin(x1, y1, x2, y2) {
  stroke("#1a1a1d");
  strokeWeight(6);
  line(x1, y1, x2, y2);

  strokeWeight(1);
  stroke("Aqua");
  line(x1, y1, x2, y2);
  eraseFlag = false;
}
//! auxiliary tools
function windowResized() {
  resizeCanvas(windowWidth / CSD, windowHeight / CSD);
  centerCanvas();
  cv.background("#1a1a1d");
}
function centerCanvas() {
  const x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  if (y < 110) {
    y = 110;
  }
  cv.position(x, y);
}
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
