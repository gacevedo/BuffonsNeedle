const http = require("http");
const express = require("express");

const app = express();
app.use(express.static("public"));

app.set("port", "3000");

const server = http.createServer(app);
server.on("listening", () => {
  console.log("Listening on port 3000");
});

// Web sockets
var pinArray = [];
var totalPins = 0;
var totalIntersects = 0;
var sendData = [0, 0, 0, 0, 0, 0, 0, 0]; // x,y,x2,y2,wdth,height,totalPins,totalIntersects
const io = require("socket.io")(server);

io.on("connection", (socket) => {
  console.log("Client connected: " + socket.id);

  socket.on("pinEvent", (data) => {
    if (data.intersect == -1) {
      //reset the world
      pinArray.length = 0;
      totalPins = 0;
      totalIntersects = 0;
    } else {
      pinArray.unshift(data); //add incoming pin to the top of the array
      totalPins++;
    }
    socket.broadcast.emit("pinEvent", data);

    console.log(data);
    console.log("x1 : ", data.intersect);

    lineCrossed = data.intersect;
    if (data.intersect == 1) {
      totalIntersects++;
    }
    sendData[0] = data.x1;
    sendData[1] = data.y1;
    sendData[2] = data.x2;
    sendData[3] = data.y2;
    sendData[4] = data.wdth;
    sendData[5] = data.hight;
    sendData[6] = totalPins;
    sendData[7] = totalIntersects;
    emitData(
      sendData[0],
      sendData[1],
      sendData[2],
      sendData[3],
      sendData[4],
      sendData[5],
      sendData[6],
      sendData[7]
    );
    function emitData(x, y, x2, y2, wdth, hight, ttlPins, ttlIntersects) {
      const dataToSend = {
        x1: x,
        y1: y,
        x2: x2,
        y2: y2,
        wdth: wdth,
        hight: hight,
        ttlPins: ttlPins,
        ttlIntersects: ttlIntersects,
      };
      console.log("emitting", dataToSend);
      io.emit("pinEmit", dataToSend);
      //socket.broadcast.emit("pinEmit", dataToSend);
    }
    function emitDataRefresh(
      x,
      y,
      x2,
      y2,
      wdth,
      hight,
      ttlPins,
      ttlIntersects
    ) {
      const dataToSend = {
        x1: x,
        y1: y,
        x2: x2,
        y2: y2,
        wdth: wdth,
        hight: hight,
        ttlPins: ttlPins,
        ttlIntersects: ttlIntersects,
      };
      io.to(socket.id).emit("pinEmit", dataToSend);
      console.log("going to emit from refresh", dataToSend);
      //socket.broadcast.emit("pinEmit", dataToSend);
    }
    console.log("total ", totalPins, " intersect: ", totalIntersects);
    if (data.first == 1) {
      console.log("array dump");
      for (i = 0; i < 300; i++) {
        // 300 is  the max number of pins
        //dataToSend.x1 = pinArray[i].x1;
        //console.log("****** aca ****** ", dataToSend);
        //socket.emit("pinEmit", dataToSend);
        newConst = pinArray[i];
        sendData[0] = newConst.x1;
        sendData[1] = newConst.y1;
        sendData[2] = newConst.x2;
        sendData[3] = newConst.y2;
        sendData[4] = newConst.wdth;
        sendData[5] = newConst.hight;

        emitDataRefresh(
          sendData[0],
          sendData[1],
          sendData[2],
          sendData[3],
          sendData[4],
          sendData[5],
          sendData[6],
          sendData[7]
        );
        console.log("const ", newConst.x1);
        console.log(i, pinArray[i]); //! replace with an emit?
        if (i >= totalPins - 1) {
          //break if less than 300
          break;
        }
      }
    } else {
      // emitData(sendData[0], sendData[1], sendData[2], sendData[3], sendData[4]);
    }
  });

  socket.on("disconnect", () => console.log("Client has disconnected"));
});

server.listen("3000");
